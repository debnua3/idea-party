Idea Party
==========

How to get started contributing! :) 
-----------------------------------

Fork the repository by clicking the ``fork`` button at gitlab.com/davidism/idea-party


Clone the repository (remember to replace [your-gitlab-username] with your actual gitlab username)::

    git clone http://git@gitlab.com/[your-gitlab-username]/idea-party
    cd idea-party

Create a virtual environment, activate it, and pip upgrade/install for ``pip``, ``setuptools``, ``wheel``, and ``ipython`` (Note: on Windows, use ``py3`` instead of ``python3`` in the code below)::

    python3 -m venv env
    . env/bin/activate
    pip install -U pip setuptools wheel ipython

Create local settings::

    mkdir instance
    python generate_instance_settings.py > instance/settings.py

Install the application in development mode::

    pip install -e .

Create the database::

    idea-party migrate

Run the server and take a look at the project::

    idea-party runserver

Quit the server when you're done looking around::

    ^C

Then make a branch for your feature and get to work :) ::

    git checkout -b my-awesome-feature
