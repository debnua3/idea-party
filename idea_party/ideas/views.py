from django.shortcuts import render, get_object_or_404
from .models import Idea
from .forms import IdeaForm
from django.shortcuts import redirect


def idea_new(request):
    if request.user.is_authenticated:
        user = request.user
        if request.method == "POST":
            form = IdeaForm(request.POST)
            if form.is_valid():
                idea = form.save(commit=False)
                idea.created_by = user
                idea.save()
                return redirect('ideas:idea_detail', pk=idea.pk)
        else:
            form = IdeaForm()
            return redirect('index')
    else:
        return redirect('index')


def idea_detail(request, pk):
    idea = get_object_or_404(Idea, pk=pk)
    return render(request, 'ideas/idea_detail.html', {'idea':idea})


def idea_edit(request, pk):
    idea = get_object_or_404(Idea, pk=pk)
    if request.user.is_authenticated and request.user==idea.created_by:
        if request.method == "POST":
            form = IdeaForm(request.POST, instance=idea)
            if form.is_valid():
                idea.save()
                return redirect('ideas:idea_detail', pk=idea.pk)
            else:
                form = IdeaForm(instance=idea)
        else:
            form = IdeaForm(instance=idea)
            return render(request, 'ideas/idea_edit.html', {'form': form})
    else:
        return redirect('ideas:idea_detail', pk=idea.pk)