import os
import pkgutil
import sys


def get_package_and_instance_paths(name):
    loader = pkgutil.get_loader(name)
    filename = os.path.abspath(loader.get_filename(name))
    package_path = os.path.dirname(filename)
    root_path = os.path.dirname(package_path)
    site_parent, site_folder = os.path.split(root_path)
    py_prefix = os.path.abspath(sys.prefix)

    if root_path.startswith(py_prefix):
        base_dir = py_prefix

    elif site_folder.lower() == 'site-packages':
        parent, folder = os.path.split(site_parent)

        if folder.lower() == 'lib':
            base_dir = parent

        elif os.path.basename(parent).lower() == 'lib':
            base_dir = os.path.dirname(parent)

        else:
            base_dir = site_parent

    else:
        return package_path, os.path.join(root_path, 'instance')

    return package_path, os.path.join(base_dir, 'var', name + '-instance')


def settings_from_file(filename, namespace, silent=False):
    try:
        with open(filename) as f:
            source = f.read()
    except IOError as e:
        if silent:
            return

        e.strerror = (
            'Unable to load configuration file ({})'
        ).format(e.strerror)
        raise

    settings = {}
    exec(compile(source, filename, 'exec'), settings)

    for key, value in settings.items():
        if key.isupper():
            namespace[key] = value


def setting_if_env(namespace, key, default=..., env_key=None, type=str):
    env_key = env_key or key

    if env_key in os.environ:
        namespace[key] = type(os.environ[env_key])
